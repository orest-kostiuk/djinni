class AccountTips
  def initialize(developer)
    @developer = developer
    @tips = []
  end

  Methods = %i[check_specializations check_projects check_photos check_education check_bio check_personal_preferences check_start_coding_year].freeze

  def process
    Methods.each do |m|
      @tips.push(send(m))
    end
    @tips
  end

  private

  def check_photos
    :empty_avatar unless @developer.avatar.attached?
  end

  def check_start_coding_year
    :empty_start_coding_year unless @developer.start_coding_year
  end

  def check_personal_preferences
    return :empty_personal_preferences if @developer.personal_preferences.blank?
    :incorrect_personal_preferences if @developer.personal_preferences.length < 100
  end

  def check_bio
    return :empty_bio if @developer.bio.blank?
    :incorrect_bio if @developer.bio.length < 200
  end

  EDUCATION_ATTRIBUTES = %i[university field_of_study degree completion_year].freeze
  def check_education
    result = []
    @developer.educations.each do |education|
      EDUCATION_ATTRIBUTES.each do |attr|
        result << education.send(attr).present?
      end
    end
    return :empty_education if result.empty?
    :incorrect_education if result.include?(false)
  end

  PROJECT_ATTRIBUTES = %i[name start_date specializations what_was_done service_list tool_list].freeze
  def check_projects
    results = []
    project_count = []
    @developer.projects.each do |p|
      PROJECT_ATTRIBUTES.each do |attr|
        results << !p.send(attr).blank?
      end
      project_count.push(!results.include?(false))
    end
    return :empty_projects if (project_count.select { |p| p if p }).count < 1
    :one_project if (project_count.select { |p| p if p }).count == 1
  end

  def check_specializations
    return :specializations unless @developer&.specializations.present?
    title_specializations = @developer.developer_specializations.map { |ds| correct_years?(ds) }.compact
    title_specializations unless title_specializations.blank?
  end

  def correct_years?(ds)
    ds.specialization.title if ds.specialization_fraction_from_all_experiences == 0
  end
end
